# Stop the modem manager
# run the modem manager debug mode
# get SIM card details using AT commands

import json
import subprocess
import threading
import time
import re
import yaml
from subprocess import DEVNULL
import os

# Execute a shell command and wait until command terminates
def subProcess(command):
    p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    p_status = p.wait()
    data = output.decode("utf-8")
    return data

# Gets the operator name using AT command
def getOperator(command):
    print(command)
    cmdOut = subProcess(command)
    opOut = ""
    if len(cmdOut) > 0:
        opOut = cmdOut.split(':')[2].split(',')[2]
    return opOut

# Gets the APN name using AT command
def getPhoneNumber(command):
    print(command)
    cmdData = subProcess(command)
    apnout = ""
    if len(cmdData) > 0:
        #apnout = cmdData.split(':')[2].split(',')[2]
        apnout = cmdData
    return apnout


# Shell commands
if __name__ == "__main__":
    # To stop the modem manager 
    subProcess("sudo systemctl stop ModemManager")
    time.sleep(2)
    # To run the modem manager in debug mode
    pMmDebug = subprocess.Popen("sudo ModemManager --debug", stdout=DEVNULL, stderr=DEVNULL, shell=True)
    print('waiting for 50 seconds to modem manager boot up...')
    for i in range(5):
        print(".", end="")
        time.sleep(10)

    # To execute AT commands and find operator name and APN name
    operatorName = getOperator("sudo mmcli -m 0 --command=\"AT+COPS?\"")
    if len(operatorName) > 0:
        print("Operator name ", operatorName)
    else:
        print("Couldn't read operator name")
    phoneNumber = getPhoneNumber("sudo mmcli -m 0 --command=\"AT+CNUM\"")
    if len(phoneNumber) > 0:
        print("Phone Number ", phoneNumber)
    else:
        print("Couldn't read Phone Number")
    print('waiting for 5 seconds command execution...')
    time.sleep(5)
    subProcess("sudo pkill -KILL ModemManager")
    time.sleep(2)
    # To start the modem manager 
    subProcess("sudo systemctl start ModemManager")
    time.sleep(10)
    # configNetplan(apnName, netcfg_filePath)